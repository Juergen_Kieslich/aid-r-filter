#include <stdlib.h>
#include "pixelCounterList.h"

PixelCounter* getPixelCounter(PixelCounter **list, Uint32 pixel){
	PixelCounter *current = *list;
	while (current != NULL){ //check if current counter and ID (value) is not null
			if(current->value == pixel){
				return current;
			}
        current=current->next;
	}
	return NULL;
}

PixelCounter* getHighestCounter(PixelCounter **list){
	PixelCounter *current = *list;
	PixelCounter *highest = current;
	while (current != NULL ){
		if(current->occured > highest->occured){
			highest = current;
		}
		current=current->next;
	}
	return highest;
}

PixelCounter* append(PixelCounter **list, Uint32 pixel)
{
    PixelCounter *current = *list;
    PixelCounter *new;
    new = ( PixelCounter*) malloc(sizeof(*new)); // create new pixelcounter
    new->value = pixel;
    new->occured = 0;
    new->next = NULL;
    new->prev = NULL;

    if ( current != NULL ) {
        while (current->next != NULL)
            current=current->next;
        new->prev = current;
	current->next=new;
    }
    else
        *list = new;
	return new;
}

void removeAll(PixelCounter **list){
	PixelCounter *current = *list;
	while (current != NULL ){
		PixelCounter *remove = current;
		current=current->next;
		if(current != NULL){
		current->prev = NULL;
		}
		free(remove);
	}
	*list = NULL;
}
