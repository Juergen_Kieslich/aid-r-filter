#include <stdlib.h>
#include "SDL/SDL.h"

int main(int argc, char *argv[]);
SDL_Surface* filter(SDL_Surface* input, SDL_Surface* ref);
SDL_Surface* downscale(SDL_Surface* input);
SDL_Surface* upscale(SDL_Surface* input);
int compareSurface(SDL_Surface* input, SDL_Surface* input2);
int comparePixel(Uint32 p1, Uint32 p2, SDL_PixelFormat *format);
SDL_Surface* createSurface(int width, int height, SDL_PixelFormat* format);
Uint32 getpixel(SDL_Surface *surface, int x, int y);
void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
