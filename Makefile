all:
	gcc -g -O2 -Wall -I /usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -g -O2 -Wall -L /usr/lib -lSDL -lpthread -lSDL_image  aidrfilter.c aidrfilter.h pixelCounterList.c pixelCounterList.h -o aidrfilter

clean:
	rm -f aidrfilter

new: clean all