#include <stdlib.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "aidrfilter.h"
#include "pixelCounterList.h"

Uint8 threshold;
int main(int argc, char *argv[])
{
    char* in = "";
    char* out =  "";
    int n = 0;
    int dstX = 0, dstY = 0;
    short dsToMinumum = 0;
//    short force = 0;
    short ignoreDScheck = 0;
    short doResize = 0;

    if(argc < 5 || argc > 9)
    {
        fprintf(stderr, "Incorrect Parameters\naidrfilter $input_path [OPTIONS] $cycles $threshold $output_path\n[Parameters]:\n $input_path = path to the source file\n $cycles: How many filtering cycles should be performed (recommanded value: 1-5)\n $threshold: Threshold for Pixel Comparision\n $output_path: path to the output file\n[Options]:\n [-d|--downsize]: downscale to lossless minimum (crops unfitting images)\n [-f|--force]: force downsize (might cause undisered effects)\n [-r x,y|--resize x,y]: will resize the output (lossy output)\n [-i|--ignore]: No downscale check . Use in case of unsatifised filtering\n");
        exit(1);
    }


    int i;
    for(i = 0; i < argc; i++)
    {
        if(i == 1)
        {
            in = argv[1];
        }
        if(argv[i][0] == '-')
        {
            while(argv[i][0] == '-')
            {
                if(strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--resize") == 0)
                {
                    i++;
                    char *p;
                    p = strtok(argv[i], ",");
                    dstX = atoi(p);
                    p = strtok(NULL, ",");
                    if(p)
                    {
                        dstY = atoi(p);
                    }
                    if(dstX)
                    {
                        doResize = 1;
                    }
                    else
                    {
                        printf("Ignore Resizing: Parameters are incorrect\n");
                    }
                }
                if(strcmp(argv[i],"-i") == 0 || strcmp(argv[i], "--ingore") == 0)
                {
                    printf("Ignoring Downsize Check\n");
                    ignoreDScheck = 1;
                }
                if(strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--downscale") == 0)
                {
                    printf("Downsize active [EXPERIMENTAL]\n");
                    dsToMinumum = 1;
                }
                if(strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--force") == 0)
                {
                    printf("Force Downsize not implemented yet!\n");
                    //force = 1;
                }
                i++;
            }
        }
        if(i == argc-3)
        {
            n =  atoi(argv[i]);
        }
        if(i == argc-2)
        {
            threshold = (uint8_t)(atoi(argv[i]));
        }
        if(i == argc-1)
        {
            out = argv[i];
        }
    }

    printf("Inputfile: %s\n", in);
    int pow = 1;
    for(i = 0; i < n; i++)
    {
        pow = pow * 2;
    }
    printf("Size Multiplikator: %i\n", pow);
    printf("Outputfile: %s\n", out);

    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        exit(1);

    }
    atexit(SDL_Quit);

    SDL_Surface *input = IMG_Load(in);
    if(!input)
    {
        fprintf(stderr, "IMG_LOAD: %s\n", IMG_GetError());
        exit(1);
    }

    printf("Surface %s: w:%d h:%d bpp:%d pitch:%d\n", "Input", input->w, input->h, input->format->BitsPerPixel, input->pitch);
    if(dsToMinumum)
    {
        SDL_Surface *dsInput = downscale(input);
        SDL_Surface *upDS = upscale(dsInput);
        SDL_SaveBMP(upDS, "upDS.bmp");
        while(compareSurface(upscale(dsInput), input))
        {
            input = dsInput;
            dsInput = downscale(dsInput);
        }
    }
    SDL_Surface *output = NULL;
    SDL_Surface* filtered;
    for(i = 0; i < n; i++)  //upscale and filter n-times
    {
        printf("Start Cycle %i\n",i+1);
        output = upscale(input); //upscales input by 2
        filtered = filter(output, input);
        printf("Downsize Check... ");
        if(ignoreDScheck || compareSurface(downscale(filtered), input))
        {
            printf("success\n");
            if(i+1 < n)  //Override Input for next iteration
            {
                input = filtered;
                input->refcount++;
            }
        }
        else printf("failed\n");
        SDL_Surface *output = input;
        output->refcount++;
        printf("End Cycle %i\n\n",i+1);

    }
    int saved = 0;
    if(doResize)
    {
        if(dstY == 0)
        {
            dstY = (output->h * dstX) / output->w;
        }
        SDL_Surface *resizeSurf = createSurface(dstX, dstY, output->format);
        SDL_Rect src= {0,0,output->w,output->h};
        SDL_Rect dst= {0,0,resizeSurf->w,resizeSurf->h};
        SDL_SoftStretch(output,&src,resizeSurf,&dst);
        saved = SDL_SaveBMP(resizeSurf, out);
	SDL_FreeSurface(resizeSurf);
    }
    else
    {
        saved = SDL_SaveBMP(output, out);
    }
    SDL_FreeSurface(input);
    SDL_FreeSurface(output);
    if(saved < 0)
    {
        fprintf(stderr, "Unable to save output: %s\n", SDL_GetError());
        exit(1);
    }
    else printf("Image saved\n");
    return 0;
}

SDL_Surface* filter(SDL_Surface* input, SDL_Surface* ref) //Does not work perfectly
{
    SDL_Surface *output = input;
    output->refcount++;
    int x, y;
    for(y = 0; y < ref->h; y++)
    {
        for(x = 0; x < ref->w; x++)
        {
            short isNorthwest;  //needed for checking if there is a pixel. pixel = 0 => transparent
            short isNortheast;
            short isSouthwest;
            short isSoutheast;
            Uint32 north;
            Uint32 east;
            Uint32 south;
            Uint32 west;
            Uint32 northeast;
            Uint32 northwest;
            Uint32 southeast;
            Uint32 southwest;


            Uint32 p = getpixel(ref, x, y);

            int _x = x*2; //x startposition of output
            int _y = y*2; //y startposition of output
            isNorthwest = (x == 0 || y == 0) ? 0 : 1; //check if there is a pixel at northwest (north ^ west)
            isNortheast = (x == ref->w - 1 || y == 0) ? 0 : 1;
            isSouthwest = (x == 0 || y == ref->h - 1) ? 0 : 1;
            isSoutheast = (x == ref->w - 1 || y == ref->h - 1) ? 0 : 1;
            if(y == 0) north = 0;
            else
            {
                north = getpixel(ref, x, y-1);
            }

            if(y == ref->h - 1) south = 0;
            else
            {
                south = getpixel(ref, x, y+1);
            }

            if(x == 0) west = 0;
            else
            {
                west = getpixel(ref, x-1, y);
            }

            if(x == ref->w - 1) east = 0;
            else
            {
                east = getpixel(ref, x+1, y);
            }

            if(!isNortheast) northwest = 0;
            else
            {
                northwest = getpixel(ref, x-1, y-1);
            }

            if(!isSoutheast) southeast = 0;
            else
            {
                southeast = getpixel(ref, x+1, y+1);
            }

            if(!isNortheast) northeast = 0;
            else
            {
                northeast = getpixel(ref, x+1, y-1);
            }

            if(!isSoutheast)  southwest = 0;
            else
            {
                southwest = getpixel(ref, x-1, y+1);
            }

            if(isNorthwest && northwest != p && (comparePixel(north, northwest, ref->format) && comparePixel(north, west, ref->format)))
            {
                putpixel(output, _x, _y, north);
            }

            if(isNortheast && northeast != p &&(comparePixel(north, northeast, ref->format) && comparePixel(north, east, ref->format)))
            {
                putpixel(output, _x+1, _y, north);
            }

            if(isSouthwest && southwest != p && (comparePixel(south, southwest, ref->format) && comparePixel(south, west, ref->format)))
            {
                putpixel(output, _x, _y+1, south);
            }

            if(isSoutheast && southeast != p && (comparePixel(south, southeast, ref->format) && comparePixel(south, east, ref->format)))
            {
                putpixel(output, _x+1, _y+1, south);
            }
        }
    }
    return output;
}

SDL_Surface* downscale(SDL_Surface* input) //Works
{
    SDL_Surface *output = createSurface(input->w/2, input->h/2, input->format);
    int x, y;
    for(x = 0; x < output->w; x++)
    {
        for(y = 0; y < output->h; y++)
        {
            int _x = x*2; //x startposition of input
            int _y = y*2; //y startposition of input
            PixelCounter *list = NULL;
            int listCount = 0;
            int i, j;
            for(i = 0; i < 2; i++)  //read 2x2 pixel
            {
                for(j = 0; j < 2; j++)
                {
                    Uint32 p = getpixel(input, _x+i, _y+j);
                    PixelCounter *pc;
                    if(listCount > 0)
                    {
                        pc = getPixelCounter(&list, p);
                        if(pc == NULL)
                        {
                            pc = append(&list, p);
                            listCount++;
                        }
                    }
                    else
                    {
                        pc = append(&list, p);
                        listCount++;
                    }
                    pc->occured++;
                }
            }
            PixelCounter *pc = getHighestCounter(&list);
            putpixel(output, x, y, pc->value);
            removeAll(&list);
        }
    }
    return output;
}

SDL_Surface* upscale(SDL_Surface* input)
{
    SDL_Surface *output = createSurface(input->w*2, input->h*2, input->format);
    int x, y;
    for(y = 0; y < input->h; y++)
    {
        for(x = 0; x < input->w; x++)
        {

            Uint32 p = getpixel(input, x, y);

            int _x = x*2; //x startposition of output
            int _y = y*2; //y startposition of output
            int i, j;
            for(i = 0; i < 2; i++)  //fill 2x2 with the current pixel of input
            {
                for(j = 0; j < 2; j++)
                {
                    SDL_LockSurface(output);
                    putpixel(output, _x+i, _y+j, p);
                    SDL_UnlockSurface(output);
                }
            }
        }
    }
    return output;
}

int compareSurface(SDL_Surface* input, SDL_Surface* input2)
{
    if(input->w != input2->w || input->w != input2->w)
    {
        return 0;
    }
    int x, y;
    for(y = 0; y < input->h; y++)
    {
        for(x = 0; x < input->w; x++)
        {
            Uint32 pF = getpixel(input, x, y);
            Uint32 pI = getpixel(input2, x, y);
            if(pF != pI)
            {
                return 0;
            }
        }
    }
    return 1;
}

int comparePixel(Uint32 p1, Uint32 p2, SDL_PixelFormat *format)
{
    Uint8 r,g,b,a, _r, _g, _b, _a;
    SDL_GetRGBA(p1, format, &r, &g, &b, &a);
    SDL_GetRGBA(p2, format, &_r, &_g, &_b, &_a);
    if((_r >= r - threshold && _r <= r+threshold) &&
            (_g >= g - threshold && _g <= g+threshold) &&
            (_b >= b - threshold && _b <= b+threshold) &&
            (_a >= a - threshold && _a <= a+threshold)) return 1;
    else return 0;
}

SDL_Surface* createSurface(int width, int height, SDL_PixelFormat* format)
{
    return SDL_CreateRGBSurface(SDL_SWSURFACE, width, height,  //SWSURFACE in favor of HWSURFACE, because pixelacess is faster
                                32, format->Rmask, format->Gmask, format->Bmask, format->Amask);
}

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;

    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
    switch(bpp)
    {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;

    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp)
    {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        }
        else
        {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;

    default:
        break;
    }
}
