#ifndef PIXELCOUNTERLIST_H
#define PIXELCOUNTERLIST_H
#include "SDL/SDL.h"

typedef struct PixelCounter PixelCounter;

struct PixelCounter{
    Uint32 value; //value of pixel
    int occured; //occurrence of the pixel in an image
    PixelCounter *next; // next pixelCounter
    PixelCounter *prev; //previous pixelCounter
};

PixelCounter *getPixelCounter(PixelCounter **list, Uint32 pixel);
PixelCounter *getHighestCounter(PixelCounter **list);
PixelCounter *append(PixelCounter **list, Uint32 pixel);
void removeAll(PixelCounter **list);

#endif
